function FindProxyForURL(url, host)
{
	var lhost = host.toLowerCase();
	host = lhost;

	if (shExpMatch(host, "*.avantassessment.com") ||
	shExpMatch(host, "*.drc-centraloffice.com") ||
	shExpMatch(host, "*.drcedirect.com") ||
	shExpMatch(host, "east-1-drc-wbte-prod-wida.s3.amazonaws.com") ||
	shExpMatch(host, "east-2-drc-wbte-prod-wida.s3.amazonaws.com") ||
	shExpMatch(host, "drc-wbte-prod.s3.amazonaws.com") ||
	shExpMatch(host, "us-east-1-content-hosting-form-locker-prod.s3.us-east-1.amazonaws.com") ||
	shExpMatch(host, "us-east-2-content-hosting-form-locker-prod.s3.us-east-2.amazonaws.com") ||
	shExpMatch(host, "*.twilio.com") ||
	shExpMatch(host, "*.apple.com") ||
	shExpMatch(host, "*.apple-dns.net") ||
	shExpMatch(host, "*.icloud.com") ||
	shExpMatch(host, "*.icloud-content.com") ||
	shExpMatch(host, "*.cdn-apple.com") ||
	shExpMatch(host, "*.gimkitconnect.com") ||
	shExpMatch(host, "*.vansd.org") ||
	shExpMatch(host, "*.digicert.com") ||
	shExpMatch(host, "*.mzstatic.com") ||
	shExpMatch(host, "*.symcb.com") ||
	shExpMatch(host, "*.symcd.com") ||
	shExpMatch(host, "*.commerce.wa.gov") ||
	shExpMatch(host, "content.govdelivery.com") ||
	shExpMatch(host, "expressoptimizer.net") ||
	shExpMatch(host, "460.global.siteimproveanalytics.io") ||
	shExpMatch(host, "*.zoom.us") ||
	shExpMatch(host, "*.seesaw.me") ||
	shExpMatch(host, "*.getepic.com") ||
	shExpMatch(host, "m.google.com") ||
	shExpMatch(host, "*.googlevideo.com") ||
	shExpMatch(host, "server.sbcccweb.com") ||
	shExpMatch(host, "5rw61tcrl5.execute-api.us-west-2.amazonaws.com") ||
	shExpMatch(host, "realtime.ably.io") ||
	shExpMatch(host, "rest.ably.io") ||
	shExpMatch(host, "global.stun.twilio.com") ||
	shExpMatch(host, "global.turn.twilio.com") ||
	shExpMatch(host, "lightspeed-realtime.ably.io") ||
	shExpMatch(host, "*-fallback-lightspeed.ably.io") ||
	shExpMatch(host, "devices.lsmdm.com") ||
	shExpMatch(host, "relay.school") ||
	shExpMatch(host, "*.relay.school") ||
	shExpMatch(host, "lsrelayaccess.com") ||
	shExpMatch(host, "*.lsfilter.com") ||
	shExpMatch(host, "p7nvu5it0k.execute-api.us-west-2.amazonaws.com") ||
	shExpMatch(host, "lsrelay-config-production.s3.amazonaws.com") ||
	shExpMatch(host, "lsrelay-extensions-production.s3.amazonaws.com") ||
	shExpMatch(host, "collegeboard.org") ||
	shExpMatch(host, "*.collegeboard.org") ||
	shExpMatch(host, "169.204.191.*") ||
	shExpMatch(host, "10.*.*.*") || 
	shExpMatch(host, "*.youtube.com") ||
	shExpMatch(host, "youtube.com"))
	{
		return "DIRECT";
	}

	return "DIRECT";
}
